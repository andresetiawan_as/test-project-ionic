import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the SecondPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-second',
  templateUrl: 'second.html',
})
export class SecondPage implements OnInit{

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ngOnInit(){
    //ambil paramter yang di parsing dari home
    console.log(this.navParams.data);
    console.log(this.navParams.get("umur"));
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SecondPage');
  }

}
