import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { SecondPage } from '../second/second';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  constructor(public navCtrl: NavController) {

  }

  goToSecondPage(){
    console.log("Masuk second page!");

    // let manusia = {}; //buat suatu json object
    // this.navCtrl.push(SecondPage, manusia);

    this.navCtrl.push(SecondPage, {nama:"Andre", umur:"21"});
  }

}
